#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include "lib.h"

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmissing-noreturn"
#define QUEUE_LEN 10
#define BUF_SIZE 256

int main(void) {
    int listener, conn_sock_fd, sock_fd, ret, flag, nbytes;
    char str_addr[INET6_ADDRSTRLEN], buffer[BUF_SIZE];
    struct sockaddr_in6 server_address, client_address;
    struct timeval tv;
    unsigned int len = sizeof(client_address);
    fd_set read_fds, master;

    FD_ZERO(&master);    // clear the master and temp sets
    FD_ZERO(&read_fds);

    listener = socket(AF_INET6, SOCK_STREAM, IPPROTO_TCP);
    /* Set listen socket non-blocking */
    flag = fcntl(listener, F_GETFL, 0);
    fcntl(listener, F_SETFL, flag | O_NONBLOCK);

    server_address.sin6_family = AF_INET6;
    server_address.sin6_addr = in6addr_any;
    server_address.sin6_port = htons(PORT);

    bind(listener, (struct sockaddr *) &server_address, sizeof(server_address));

    if (listen(listener, QUEUE_LEN) == -1) {
        perror("Listen ");
        return EXIT_FAILURE;
    }
    FD_SET(listener, &master);
    printf("Running\n");
    while (1) {
        read_fds = master;
        tv.tv_sec = 1;
        tv.tv_usec = 0;

        ret = select(FD_SETSIZE, &read_fds, NULL, NULL, &tv);
        if (ret == -1) {
            perror("select(): ");
        } else if (ret > 0) {
            for (sock_fd = 0; sock_fd < FD_SETSIZE; sock_fd++) {
                if (FD_ISSET(sock_fd, &read_fds)) {
                    if (sock_fd == listener) {
                        //accept new connection
                        conn_sock_fd = accept(listener, (struct sockaddr *) &client_address, &len);
                        if (conn_sock_fd == -1)
                            perror("accept new connection");
                        else {
                            FD_SET(conn_sock_fd, &master);
                            // Set connection socket non-blocking too
                            flag = fcntl(conn_sock_fd, F_GETFL, 0);
                            fcntl(listener, F_SETFL, flag | O_NONBLOCK);
                            //Get client address
                            inet_ntop(AF_INET6, &client_address.sin6_addr, str_addr, sizeof(client_address));
                            printf("New connection from: %s:%d\n", str_addr, client_address.sin6_port);
                        }
                    } else {
                        if ((nbytes = (int) recv(sock_fd, buffer, BUF_SIZE, 0)) <= 0) {
                            if (nbytes == 0)
                                printf("Socket %d disconnected\n", sock_fd);
                            else
                                perror("recvfrom(): ");
                            close(sock_fd); //close the socket used by a user who terminated the ./client application
                            FD_CLR(sock_fd, &master);
                        } else {
                            //printf("%s", buffer); // debug: printing the user input that got to the server
                            for (int i = 0; i < FD_SETSIZE; i++)
                                if (FD_ISSET(i, &master))
                                    if (i != listener && i != sock_fd) //dont send back the message to the sender
                                        if (send(i, buffer, BUF_SIZE, 0) == -1)
                                            perror("send to clients");
                        }
                    }
                }
            }
        }
    }
    return EXIT_SUCCESS;
}

#pragma clang diagnostic pop