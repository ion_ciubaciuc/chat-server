#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <netdb.h>
#include <pthread.h>

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmissing-noreturn"
#pragma clang diagnostic ignored "-Wint-to-void-pointer-cast"
#define SERVICE "7777"
#define BUF_SIZE 256
#define MESS_SIZE 246

/**
 *This method will create a thread for receving and printing messages
 *from other clients while waiting for user input
 */
void *receiveMessage(void *socket) {
    int sock_fd, ret;
    char buffer[BUF_SIZE];
    sock_fd = (int) socket;
    memset(buffer, 0, BUF_SIZE);
    while (1) {
        if ((int) recv(sock_fd, buffer, BUF_SIZE, 0) < 0)
            printf("Error receiving data!\n");
        else
            printf("%s", buffer);
    }
}

int main(int argc, char const *argv[]) {
    int sock_fd, ret;
    struct addrinfo hints, *result, *rp;
    char str_addr[INET6_ADDRSTRLEN];
    int len6 = sizeof(struct sockaddr_in6);
    int len4 = sizeof(struct sockaddr_in);

    char buffer[BUF_SIZE];
    char nick[10];
    char message[MESS_SIZE];
    pthread_t readTh;

    if (argc != 3) {
        printf("Syntax: %s server_address nick_name\n", argv[0]);
        return EXIT_FAILURE;
    }

    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_family = AF_UNSPEC;        /* Allow IPv4 or IPv6 */
    hints.ai_socktype = SOCK_STREAM;    /* Allow stream protocol */
    hints.ai_flags = 0;                    /* No flags required */
    hints.ai_protocol = IPPROTO_TCP;    /* Allow TCP protocol only */

    if ((ret = getaddrinfo(argv[1], SERVICE, &hints, &result)) != 0) {
        perror("getaddrinfo(): ");
        return EXIT_FAILURE;
    }

    /* Try to use addrinfo from getaddrinfo() */
    for (rp = result; rp != NULL; rp = rp->ai_next) {
        sock_fd = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
        if (rp->ai_family == AF_INET) {
            if (inet_ntop(AF_INET, &((struct sockaddr_in *) rp->ai_addr)->sin_addr, str_addr, len4) != NULL)
                printf("Trying IPv4 address: %s:%s ...\n", str_addr, SERVICE);
            else
                printf("Not valid IPv4 address.\n");
        } else if (rp->ai_family == AF_INET6) {
            if (inet_ntop(AF_INET6, &((struct sockaddr_in6 *) rp->ai_addr)->sin6_addr, str_addr, len6) != NULL)
                printf("Trying IPv6 address: [%s]:%s ...\n", str_addr, SERVICE);
            else
                printf("Not valid IPv6 address\n");
        }
        /* Do TCP handshake */
        if (connect(sock_fd, rp->ai_addr, rp->ai_addrlen) != -1)
            break;
        else
            printf("Failed\n");
        close(sock_fd);
        sock_fd = -1;
    }

    if (rp == NULL) {
        printf("Could not connect to the [%s]:%s\n", argv[1], SERVICE);
        freeaddrinfo(result);
        return EXIT_FAILURE;
    }
    printf("Connected!\n");
    memset(buffer, 0, BUF_SIZE);

    ret = pthread_create(&readTh, NULL, receiveMessage, (void *) sock_fd); //create the thread for reciving messages
    if (ret) {
        printf("ERROR: Return Code from pthread_create() is %d\n", ret);
        return EXIT_FAILURE;
    }
    printf("Your text: ");
    while (1) {
        fgets(message, MESS_SIZE, stdin); //get the user message
        strncpy(nick, argv[2], 10); //copy second argument from cmdln to variable nick(name)
        sprintf(buffer, "%s%c %s", nick, ':', message); //concatenate the nick(name) with the message
        if ((int) sendto(sock_fd, buffer, BUF_SIZE, 0, (struct sockaddr *) &rp, sizeof(rp)) < 0) //send the message
            printf("Error sending data!\n\t-%s", buffer);
    }

    close(sock_fd);
    pthread_exit(NULL);

    return EXIT_SUCCESS;
}

#pragma clang diagnostic pop